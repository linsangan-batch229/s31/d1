//require() - is a built-in JS method which allows us to import packages.
//Packages are pieces of code we can integrate into our application
const http = require("http");

//http modulele lets us create a server which is able to communicate with
//a client through the use the HTTP
//"http" is a default package that comes with NodeJS. It allows us to use method that
//let us create servers
//http - is now a module in your application that works much like an object

//when you added the URL to the client (browser), the client made a rquest,
//to the server, localHost:4000

//http - protocol, ginagamit ng server/client para mag uusap,
//kung saan sila mag uusap, which is HTTP

//localhost:4000/ - localhost - means your local server/machine
//4000 - is the port/the process number in your computerr

//when you added the url http://localhost:4000/, the client triggered
//the application on localhost:4000, our node-ks

console.log(http);

http.createServer(function(request,response){
    response.writeHead(200, {'Content-Type' : 'text/plain'});
    response.end("Hello from our first Node JS Server");
}).listen(4000);

//liste() allows us to assign a port to our server. This will allow us
//to serve/run our index.js server in our local machine to port 4000

console.log("Server is running on localhost:4000!");



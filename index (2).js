// const http = require("http");
// const { connected } = require("process");

// http.createServer(function(request,response){
//     response.writeHead(200, {'Content-Type' : 'text/plain'});
//     response.end("String only data type");
// }).listen(4000);

// onsole.log("Server is running on localhost:4000!")

const http = require('http');
const port = 3000;
const server = http.createServer((require, response) => {
    
    //nag request ng url
    // '/greeting' -> end point
    if (require.url == "/greeting"){
        //http codes
        //200 - sucessful yng request from browser to server
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end('This is the greeting');
    } else if (require.url == "/homepage") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end('This is the homepage')

    } else {
        response.writeHead(404, {"Content-Type" : "text/plain"});
        response.end('404 - Page not found');
    }

});

server.listen(port);
console.log(`Server now accessible at localhost: ${port}`);
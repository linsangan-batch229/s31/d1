//require() - is a built-in JS method which allows us to import packages.
//Packages are pieces of code we can integrate into our application
const http = require("http");
const { connected } = require("process");

//http modulele lets us create a server which is able to communicate with
//a client through the use the HTTP
//"http" is a default package that comes with NodeJS. It allows us to use method that
//let us create servers
//http - is now a module in your application that works much like an object

//when you added the URL to the client (browser), the client made a rquest,
//to the server, localHost:4000

//http - protocol, ginagamit ng server/client para mag uusap,
//kung saan sila mag uusap, which is HTTP

//localhost:4000/ - localhost - means your local server/machine
//4000 - is the port/the process number in your computerr

//when you added the url http://localhost:4000/, the client triggered
//the application on localhost:4000, our node-ks



http.createServer(function(request,response){
    response.writeHead(200, {'Content-Type' : 'text/plain'});
    response.end("String only data type");
}).listen(4000);

//liste() allows us to assign a port to our server. This will allow us
//to serve/run our index.js server in our local machine to port 4000

//4000, 4040, 

console.log("Server is running on localhost:4000!");


//createserver() - is a method from the http module. it allow us to create
//a server that is able to handle the request of a client and send a response
//to the client

//createServer() has a funcion as an argument. This functuin handles
//the handles the reqquest and the response. the request object contains
//details of the request from the client. THe response object contains
//details of the response from the server. the http.createServer() method
//always recieves the request object first before the response

//http.createServer('request', 'response') - request muna lagi before response

//respose.writerHead() 
    // - is a method of the response Object. It allows us to add headers to our 
    // response. headers are additional information about our response. We have 2
    // argumetns in writeHead() method, first is the http status connected. an http
    // status is just a numerical code to tell the 404 means the resource cannot be 
    // found
    // response.writeHead(200, {'Content-Type' : 'text/plain'})
    //     - 1st argument - 200 - status code 
    //     - 2nd argument - data type of the response / actual header
    //         - allows us to create what kind of data is the response


    // response.end("Hello from our first Node JS Server");
    //     - it ends our response. Meaning no more processes/tasks after response.end
    //     - also able to send a msg or data as a string
    